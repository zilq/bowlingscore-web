import React from 'react';
import './Table.css';
import Button from 'react-bootstrap/Button';

class Table extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      rows: [],
      rowsReady: false,
      url: process.env.REACT_APP_BASE_URL
    };
    this.generateTableRows = this.generateTableRows.bind(this);
    this.reset = this.reset.bind(this);
    this.quit = this.quit.bind(this);
    this.makeThrow = this.makeThrow.bind(this);
    this.getTotalScore = this.getTotalScore.bind(this);
  }

  componentDidMount() {
    this.generateTableRows();
    this.setState({
      currentPlayer: this.props.players[0].id,
      currentPlayerPoints: [],
      currentFrame: 1,
      previousPlayer: -1
    });
    this.initGame();
  }

  initGame() {
    this.currentChance = 1;
    this.pinsStanding = 10;
    this.points = [];
  }

  generateTableRows() {
    fetch(this.state.url + "/frames")
    .then(response => response.json())
    .then(data => {
      this.setState({frames: this.getFramesByPlayerId(data), rowsReady: false});
      this.buildRows();
    });
  }

  getFramesByPlayerId(dtos) {
    const players = this.props.players;
    const framesByPlayerId = {};

    for (const player of players) {
      const playerFrames = dtos.find(dto => dto.playerId === player.id).frames;
      framesByPlayerId[player.id] = playerFrames;
    }
    
    return framesByPlayerId;
  }

  buildRows() {
    const players = this.props.players;
    let rows = [];
    rows.push(this.generateTableHeader());
    for (const player of players) {
      rows = rows.concat(this.generateRow(player));
    }
    this.setState({rows: rows, rowsReady: true});
  }

  generateTableHeader() {
    const headers = [<th colSpan='6' key="frame_player_name" className="header_column">Player</th>];
    for (let i = 1; i <= 10; i++) {
      headers.push(<th colSpan='6' key={"frame_" + i} className="header_column">#{i}</th>);
    }
    headers.push(<th colSpan='6' key="total" className="header_column">Total</th>);
    return (<tr key="headers">{headers}</tr>);
  }

  generateRow(player) {
    let points = [<td colSpan='6' key={"f_player_" + player.id} className="player_column_top">{player.name}</td>];
    const score = [<td colSpan='6' key={"s_player_" + player.id} className="player_column_bottom"></td>];
    const frames = this.state.frames[player.id];
    for (let i = 0; i < 9; i++) {
      const frame = frames[i];
      points.push(<td colSpan='3' key={"f1_" + i} className="points_column">{this.getFrameChanceValue(frame, 1)}</td>);
      points.push(<td colSpan='3' key={"f2_" + i} className="points_column">{this.getFrameChanceValue(frame, 2)}</td>);
      score.push(<td colSpan='6' key={"score_" + i} className="score_column">{this.getFrameScoreValue(frame)}</td>);
    }
    points = points.concat([
      <td colSpan='2' key="f1_10" className="points_column">{this.getFrameChanceValue(frames[9], 1)}</td>,
      <td colSpan='2' key="f2_10" className="points_column">{this.getFrameChanceValue(frames[9], 2)}</td>,
      <td colSpan='2' key="f3_10" className="points_column">{this.getFrameChanceValue(frames[9], 3)}</td>,
      <td colSpan='6' key="total_score" className="sum_column_top"></td>
    ]);
    score.push(<td colSpan='6' key="score_10">{this.getFrameScoreValue(frames[9])}</td>);
    score.push(<td colSpan='6' key="total_score_value" className="sum_column_bottom">{this.getTotalScore(frames)}</td>)

    return [<tr key={"points_" + player.id}>{points}</tr>, <tr key={"score_" + player.id}>{score}</tr>];
  }

  getFrameChanceValue(frame, chanceNr) {
    if (frame === undefined || frame.points.length === 0 || frame.points[chanceNr - 1] === undefined) {
      return '';
    }

    if (frame.points[chanceNr - 1] === 10) {
      return 'X';
    }

    const sum = frame.points.reduce((a, b) => a + b, 0);
    if (sum === 10 && chanceNr  === 2) {
      return "/";
    }

    return frame.points[chanceNr - 1];
  }

  getFrameScoreValue(frame) {
    if (frame === undefined) {
      return '';
    }

    return frame.score;
  }

  getTotalScore(frames) {
    return frames.map(frame => frame.score).reduce((a, b) => a + b, 0);
  }

  reset() {
    fetch(this.state.url + "/game/reset")
    .then(() => {
      this.initGame();
      this.generateTableRows();
      this.setState({gameOver: false, currentFrame: 1});
    });
  }

  quit() {
    fetch(this.state.url + "/game/quit")
    .then(() => this.props.onGameOver());
  }

  makeThrow() {
    const currentPlayer = this.state.currentPlayer;
    const currentFrame = this.state.currentFrame;
    const pinsHit = this.getRandomInteger(this.pinsStanding);
    this.points.push(pinsHit);
    this.pinsStanding -= pinsHit;
    this.updateFrame(this.points);
    if (!this.hasMoreChances(currentFrame, this.points)) {
      fetch(this.state.url + "/frames", {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(this.getFrameDto(this.points))
      }).then(() => {
        this.generateTableRows();
        const players = this.props.players;
        const nrOfPlayers = players.length;
        let newFrame = (players[nrOfPlayers - 1].id === currentPlayer) ? currentFrame + 1 : currentFrame;
        let newPlayer = (players[nrOfPlayers - 1].id === currentPlayer) ? players[0].id : currentPlayer + 1;
        if (currentFrame === 10 && players[players.length - 1].id === currentPlayer) {
          this.setState({gameOver: true});
        } else {
          this.setState({currentPlayer: newPlayer, currentFrame : newFrame});
          this.initGame();
        }
      })
    }

    this.currentChance++;
  }

  getRandomInteger(max) {
    return Math.floor(Math.random() * (max + 1 - 0) + 0);
  }

  updateFrame(points) {
    const currentPlayerFrame = this.getCurrentFrame();
    currentPlayerFrame.points = points;
    currentPlayerFrame.score = points.reduce((a, b) => a + b, 0);
    this.setState({frames: this.state.frames});
    this.buildRows();
  }

  getCurrentFrame() {
    const currentPlayer = this.state.currentPlayer;
    const currentFrameNr = this.state.currentFrame;
    const frames = this.state.frames;
    const playerFrames = frames[currentPlayer];
    if (playerFrames.length >= currentFrameNr) {
      return playerFrames[currentFrameNr - 1];
    } else {
      const newFrame = {};
      playerFrames.push(newFrame);
      this.setState({frames: frames});
      return newFrame;
    }
  }

  hasMoreChances(frameNr, pointsInCurrentFrame) {
    const pointsSum = pointsInCurrentFrame.reduce((a, b) => a + b, 0);
    if (frameNr < 10) {
      return pointsInCurrentFrame.length < 2 && pointsSum < 10;
    }

    return (pointsInCurrentFrame.length === 2 && pointsSum >= 10) || pointsInCurrentFrame.length === 1;
  }

  getFrameDto(points) {
    const pointsSum = points.reduce((a, b) => a + b, 0);
    return {
      playerId: this.state.currentPlayer,
      frame: {
        number: this.state.currentFrame,
        score: pointsSum,
        points: points
      }
    }
  }


  render() {
    const rows = this.state.rows;
    return (
      <div>
      {this.state.rowsReady &&
        <div>
        <table cellPadding='1' cellSpacing='0'>
          <tbody>
            {rows}
          </tbody>
        </table>
        <Button className="btn-primary game-button" onClick={this.makeThrow} disabled={this.state.gameOver}>NEXT THROW</Button>
        <Button className="btn-primary game-button" onClick={this.reset}>RESET</Button>
        <Button className="btn-primary game-button" onClick={this.quit}>QUIT</Button>
        </div>
      }
      </div>
    );
  }
}

export default Table;
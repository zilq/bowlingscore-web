import React from 'react';
import './Menu.css';

class Menu extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      players: [],
      playerList: [],
      url: process.env.REACT_APP_BASE_URL
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.addPlayer = this.addPlayer.bind(this);
    this.deletePlayer = this.deletePlayer.bind(this);
    this.startGame = this.startGame.bind(this);
  }

  handleNameChange(name) {
    this.setState({name: name.target.value});
  }

  addPlayer() {
    const name = this.state.name;
    const players = this.state.players;
    if (name !== "") {
      fetch(this.state.url + "/players?name=" + name, {method: "POST"})
      .then(response => response.json())
      .then(data => {
        players.push(data);
        this.setState({name: "", players: players});
        this.createList();
      });
    }
  }

  deletePlayer(id) {
    fetch(this.state.url + "/players?id=" + id, {method: "DELETE"})
    .then(() => {
      const players = this.state.players;
      const index = this.findPlayerListIndex(id);
      if (index > -1) {
        players.splice(index, 1);
        this.setState({players: players});
      }
      this.createList();
    })
  }

  findPlayerListIndex(id) {
    for (const [index, player] of this.state.players.entries()) {
      if (player.id === id) {
        return index;
      }
    }

    return -1;
  }

  startGame() {
    this.props.onSelectionComplete(this.state.players);
  }

  createList() {
    const items = [];
    for (const [index, value] of this.state.players.entries()) {
      items.push(
      <li key={index}>
        {value.name} <span className="delete-button" type="button" onClick={() => this.deletePlayer(value.id)}>x</span>
      </li>)
    }
    this.setState({playerList: items});
  }

  render() {
    return (
      <div>
        <h2>ADD PLAYER</h2>
        <div className="input-group">
          <input type="text" className="form-control" placeholder="Name" onChange={this.handleNameChange} id="name" value={this.state.name}/>
          <button className="btn-primary" type="button" onClick={this.addPlayer}>+</button>
        </div>
        {this.state.players.length > 0 &&
          <div>
          <h2>PLAYERS</h2>
          <ol>
            {this.state.playerList}
          </ol>
          <button className="btn-primary" type="button" onClick={this.startGame}>START GAME</button>
          </div>
        }
      </div>
    );
  }
}

export default Menu;
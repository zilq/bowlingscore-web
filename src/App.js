import './App.css';
import React from 'react';
import Menu from './menu/Menu';
import Table from './score-table/Table';
import Button from 'react-bootstrap/Button';
require('dotenv').config()

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isStarted: false,
      isPlayerSelection: false,
      players: [],
      gameOver: false
    }
    this.startPlayerSelection = this.startPlayerSelection.bind(this);
    this.startGame = this.startGame.bind(this);
    this.quitGame = this.quitGame.bind(this);
  }

  startPlayerSelection() {
    this.setState({isPlayerSelection: true, gameOver: false});
  }

  startGame(players) {
    this.setState({isStarted: true, isPlayerSelection: false, players: players, gameOver: false});
  }

  quitGame() {
    this.setState({gameOver: true, isStarted: false});
  }

  render() {
    const isStarted = this.state.isStarted;
    const isPlayerSelection = this.state.isPlayerSelection;
    const gameOver = this.state.gameOver;
    return (
      <div className="App">
        <h1 className="title">BOWLING</h1>
        {((!isPlayerSelection && !isStarted) || gameOver) && <Button className="btn-primary" onClick={this.startPlayerSelection}>BEGIN</Button>}
        {isPlayerSelection && <Menu onSelectionComplete={this.startGame}/>}
        {isStarted && !gameOver && <Table players={this.state.players} onGameOver={this.quitGame}/>}
      </div>
    );
  }

}

export default App;


